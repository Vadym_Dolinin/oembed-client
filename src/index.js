const { of } = require('rxjs/observable/of');
const { ajax } = require('rxjs/observable/dom/ajax');
const { map, mergeMap, catchError, tap } = require('rxjs/operators');

const detect = require('oembed-helpers');
const { Providers } = require('oembed-helpers/src/Provider');

const { tryToGetIFrameAttributes } = require('./helpers');

let API_URL = null;
const getAPIEndPointURL = (url) => `${API_URL}/?url=${url}`;

module.exports.inject = (APIURL) => {
  API_URL = APIURL;
};

module.exports.oEmbedClient = function (url) {
  if (API_URL === null) {
    console.error(`API URL must be provided. Please call "inject" function`);
    return of(null);
  }

  return of(detect(url)).pipe(
    mergeMap((data) => {
      if (data === null) {
        return of(null);
      }
      return ajax({ url: getAPIEndPointURL(url), crossDomain: true }).pipe(
        map(({ response }) => response),
        map((oEmbed) => Object.assign({}, oEmbed, { iframe: tryToGetIFrameAttributes(oEmbed.html) })),
        catchError(() => of(null))
      )
    }),
    catchError(() => of(null)),
    tap((d) => console.log(d))
  );
};

module.exports.Providers = Providers;
