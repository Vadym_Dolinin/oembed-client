const forbiddenAttributes = ['width', 'height', 'style'];

/**
 * Parser IFrame attributes
 *
 * @param {string} html
 * @param {string[]} [forbidden=['width', 'height', 'style']]
 * @returns
 */
module.exports.tryToGetIFrameAttributes = (html, forbidden = forbiddenAttributes) => {
  try {
    const parser = new DOMParser();
    const iframe = parser.parseFromString(html, 'text/html').querySelector('iframe');

    const attributes = [...iframe.attributes]
      .filter(({ name }) => !~forbidden.indexOf(name))
      .reduce((acc, a) => Object.assign({}, acc, { [a.name]: a.value }), {})
      ;

    return attributes;
  } catch (e) {
    return null;
  }
}
